import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards,Request } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto, UpdateAuthDto, LoginDto, RegisterUserDto  } from './dto/index.dto'
import { AuthGuard } from './guards/auth.guard';
import { LoginRespose } from './interfaces/loginResponse.interface';
import { User } from './entities/user.entity';


@Controller('auth')
export class AuthController {
  jwtService: any;
  constructor(private readonly authService: AuthService) {}

  @Post()
  create(@Body() createAuthDto: CreateUserDto) {
    return this.authService.create(createAuthDto);
  }

  @Post('/register')
  register(@Body() registerUserDto:RegisterUserDto){
    return this.authService.register(registerUserDto);
  }
  
  @Post('/login')
  login(@Body() loginDto:LoginDto){
    return this.authService.login(loginDto);
  }

  @UseGuards(AuthGuard)
  @Get()
  findAll( @Request() req:Request) {
    // const user = req['user'];
    // return user
    return this.authService.findAll();
  }

  @UseGuards(AuthGuard)
  @Get('check-token')
  checkToken(@Request() req:Request):LoginRespose{

    const user = req['user'] as User;
    // console.log(user._id);
    
    return {
      user,
      token:this.authService.getJwtToken({id:user._id})
    };
    
  }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.authService.findOne(+id);
//   }

//   @Patch(':id')
//   update(@Param('id') id: string, @Body() updateAuthDto: UpdateAuthDto) {
//     return this.authService.update(+id, updateAuthDto);
//   }

//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.authService.remove(+id);
//   }
}
