import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayLoad } from '../interfaces/jwtPayload.interface';
import { AuthService } from '../auth.service';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private jwtService: JwtService,
              private authService: AuthService) {}



  async canActivate(context: ExecutionContext): Promise<boolean>{
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);
    // console.log({token});
    if (!token) {
      throw new UnauthorizedException('no hay token');
    }
    try {
      const payload = await this.jwtService.verifyAsync<JwtPayLoad>(
        token,{secret: process.env.SECRET_JWT}
      );
      // console.log({payload});
      const user = await this.authService.findUserById(payload.id);

      if(!user) throw new UnauthorizedException('User does not exists');
      if(!user.isActive) throw new UnauthorizedException('User does not active');
      // 💡 We're assigning the payload to the request object here
      // so that we can access it in our route handlers
      request['user'] = user;
    } catch {
      throw new UnauthorizedException();
    }

    return Promise.resolve(true);
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers['authorization'].split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
